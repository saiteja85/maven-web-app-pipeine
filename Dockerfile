FROM tomcat:7-jdk8-corretto
#FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=/target/*.jar
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT [“java”,”-jar”,”/app.jar”]
COPY target/maven-web-application*.war /usr/local/tomcat/webapps/maven-web-application.war

